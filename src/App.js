import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col } from 'reactstrap';

import Thumbs from "./Thumbs";
import Tricolor from "./Tricolor"
import Fotos from './Fotos';
import { CIUTATS_CAT_20K } from './datos';
import Combo from './Combo';
import Previsio from "./Previsio";
import Cards from './Cards';
import 'bootstrap/dist/css/bootstrap.min.css';

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

export default class Example extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <BrowserRouter>
        <>
          <Navbar color="light" light expand="md">
            <NavbarBrand href="/">Ejercicios</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink><Link to="/">Home</Link></NavLink>
                </NavItem>
                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle nav caret>
                    Ejercicios
                </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem>
                      <Link to="/Thumbs">Thumbs</Link>
                    </DropdownItem>
                    <DropdownItem>
                      <Link to="/Tricolor">Tricolor</Link>
                    </DropdownItem>
                    <DropdownItem>
                      <Link to="/Fotos">Fotos</Link>
                    </DropdownItem>
                    <DropdownItem>
                      <Link to="/Previsio">Previsio</Link>
                    </DropdownItem>
                    <DropdownItem>
                      <Link to="/Cards">Cards</Link>
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>
                      Reset
                  </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
            </Collapse>
          </Navbar>
          <Row>
            <Col>
              <Switch>
                <Route exact path="/" render={() => <h1>inicio</h1>} />
                <Route exact path="/Thumbs" render={() => <Thumbs estadoInicial={true} />} />
                <Route exact path="/Tricolor" component={Tricolor} />
                <Route exact path="/Fotos" component={Fotos} />
                <Route exact path="/Previsio" component={Previsio} />
                <Route exact path="/Cards" component={Cards} />
                {/* <Route component={NotFound} /> */}
              </Switch>

            </Col>
          </Row>
        </>
      </BrowserRouter>
    );
  }
}




// export default () => (
//   <BrowserRouter>
//     <>
//       <Container>
//         <Row>
//           <Col md="6">
//             <ul>
//               <li><Link to="/">Home</Link></li>
//               <li><Link to="/Thumbs">Thumbs</Link></li>
//               <li><Link to="/Tricolor">Tricolor</Link></li>
//               <li><Link to="/Fotos">Fotos</Link></li>
//               <li><Link to="/Previsio">Previsio</Link></li>
//             </ul>
//           </Col>
//           <Col md="6">
//             <Switch>
//               {/* <Route exact path="/" component={Home} /> */}
//               <Route exact path="/Thumbs" render={() => <Thumbs estadoInicial={true} />} />
//               <Route exact path="/Tricolor" component={Tricolor} />
//               <Route exact path="/Fotos" component={Fotos} />
//               <Route exact path="/Previsio" component={Previsio} />
//               {/* <Route component={NotFound} /> */}
//             </Switch>
//           </Col>
//         </Row>

//       </Container>
//       {/* * <h1>Welcome to React Parcel Micro App!</h1>
//     <p>Hard to get more minimal than this React app.</p>
//     <Thumbs estadoInicial={true}/>
//     <Tricolor/>

//     <Fotos/> 

//     <Combo items = {CIUTATS_CAT_20K}/> 

//       <Previsio /> */}
//     </>
//   </BrowserRouter>
// );
