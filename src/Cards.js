import React from 'react';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button,
  Container, Row, Col
} from 'reactstrap';


export default class Cards extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      results: []
    }
    this.getPersonas = this.getPersonas.bind(this);
    this.getPersonas();
  }

  getPersonas() {
    let numeroPersonas = "10";
    let apiUrl = `https://randomuser.me/api/?results=${numeroPersonas}`;

    fetch(apiUrl)
      .then(response => response.json())
      .then(data => { this.setState(data) })
      .catch(error => console.log(error));
  }

  render() {
    if (!this.state.results.length) {
      return <h1>Cargando datos...</h1>
    }
    console.log(this.state.results)
    for (let data in this.state.results) {
      let persona = this.state.results[data]
      console.log(persona);
      console.log(persona.email );

      <Col xs="6" md="4" lg="3">
        <Card>
          <CardImg top width="100%" src="persona.picture.thumbnail" alt="Card image cap" />
          <CardBody>
            <CardTitle>persona.name.title+" "+ persona.name.first+ " "+persona.name.last</CardTitle>
            <CardSubtitle>Card subtitle</CardSubtitle>
            <CardText>`Email`${persona.email}</CardText>
            <Button>Button</Button>
          </CardBody>
        </Card>
      </Col>
    }
    
    let persona = this.state.results.map(item=>{
      let llave = item.login.username;
      console.log(item);
      <Col xs="6" md="4" lg="3" key = {llave}>
        <Card>
          <CardImg top width="100%" src="persona.picture.thumbnail" alt="Card image cap" />
          <CardBody>
            <CardTitle>item.name.title+" "+ item.name.first+ " "+item.name.last</CardTitle>
            <CardSubtitle>Card subtitle</CardSubtitle>
            <CardText>`Email`${item.email}</CardText>
            <Button>Button</Button>
          </CardBody>
        </Card>
      </Col>
    });


    return (
      <div>
        <Container>
          <Row>
            <h1>Hola</h1>
            {persona}
          </Row>
        </Container>
      </div>
    );
  }

}

