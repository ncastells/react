import React from 'react';

export default class Combo extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            nombre: "",
            texto:""
        }
        this.cambiaFiltro = this.cambiaFiltro.bind(this);
        this.texto = this.texto.bind(this);
    }

    cambiaFiltro(event){
        this.setState({
            nombre: event.target.value
        })
    }

    
    texto(event){
        this.setState({
            texto: event.target.value
        });
    }


    render(){
        
        let comarcas = this.props.items.map(el=>el.comarca)
        comarcas = [...new Set (comarcas)];
        console.log(comarcas);
        let comarcasSeleccion = comarcas.map(item => <option key={item} value = {item}>{item}</option>)
       
        let itemsFiltrados = this.props.items
        .filter(item => item.comarca === this.state.nombre)
        .map(item => <option key={item.municipi}>{item.municipi}</option>);
        console.log(itemsFiltrados)
        return (
            <div className="combo">
                <select onChange = {this.cambiaFiltro}>{comarcasSeleccion}</select>
                <select onChange ={this.texto}>{itemsFiltrados}</select>
                <h3>{this.state.texto}</h3>
            </div>
        );
    }

}