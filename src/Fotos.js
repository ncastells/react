import React from 'react';
import './css/fotos.css';
import bici from './img/bicicleta.jpg';
import moto from './img/moto.jpg';
import coche from './img/coche.jpg';
import bus from './img/bus.jpg';



export default class Fotos extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            array:[bici, moto,coche,bus],
            opciones:0
        }
         this.opciones = this.opciones.bind(this);
    }

    opciones() {
        this.setState({
            opciones:event.target.value
        })
    }

    render() {
        let imagen = this.state.array[this.state.opciones];
        return (
            <div className="fotos">
                <select onChange = {this.opciones} value={this.state.opciones}>
                    <option value="0">Bici</option>
                    <option value="2">Coche</option>
                    <option value="1">Motos</option>
                    <option value="3">Bus</option>
                </select>
                <img src = {imagen}/>
            </div>
        );
    }

}