
import React from 'react';
import TiempoSolo from './TiempoSolo';

export default class Previsio extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            list: []
        };

        this.getPrevisio = this.getPrevisio.bind(this);
        this.getPrevisio();
    }

    getPrevisio(){
        const ciutat = "barcelona,es";
        const apiKey = "84dbcf8c3480649bce9d4bb58da44b4e";
        const funcio = "forecast";
        const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;

        fetch(apiUrl)
            .then(response => response.json())
            .then(data => this.setState({list: data.list}))
            .then(data => this.setState(data))
            .catch(error => console.log(error));
    }

    render() {

        if (!this.state.list.length) {
            return <h1>Cargando datos...</h1>
        }

        let fecha = new Date(this.state.list[0].dt * 1000);
        let fecha2 = new Date(this.state.list[8].dt * 1000);
        console.log(this.state.list[0].weather[0].icon)
        let imagenUrl = `http://openweathermap.org/img/w/${this.state.list[0].weather[0].icon}.png`;
        let lista0 = this.state.list[0];
        return (
            <div>
                <TiempoSolo lista={this.state.list[0]}/>
                <TiempoSolo lista={this.state.list[8]}/>
                <TiempoSolo lista={this.state.list[16]}/>
                <TiempoSolo lista={this.state.list[24]}/>
                <TiempoSolo lista={this.state.list[32]}/>


            </div>
        );
    }
}
