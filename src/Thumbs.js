import React from 'react';
import './css/thumbs.css';

export default class Thumbs extends React.Component{
    
    constructor(props){
        super(props);
        this.state = {
            estado : this.props.estadoInicial
        }
        this.pulsar = this.pulsar.bind(this);
    }
    
    pulsar(){
        this.setState({
            estado: !this.state.estado
        })
    }

    render(){
        let claseIcono = (this.state.estado) ? "fas fa-thumbs-up" : "fas fa-thumbs-down" ;
        let mensaje = (this.state.estado) ? "muy bien" : "animos" ;
        return (
            <div onClick={this.pulsar}>
                <i className={claseIcono} />
                <span>{mensaje}</span>
            </div>
        );
    }

}