import React from 'react';
import './css/tiempoSolo.css'

const TiempoSolo = (props) => {
    let fecha = new Date(props.lista.dt*1000);
    let url = `http://openweathermap.org/img/w/${props.lista.weather[0].icon}.png`;
    
    return (
        <div className = 'tiempo'>
            <h4>{fecha.toString().substr(0,3)}</h4>
            <div>
                <img src={url}/>
            </div>
            <div>
                <h5>{props.lista.main.temp_max}</h5>
                <h5>{props.lista.main.temp_min}</h5>
            </div>
        </div>
    )
}

export default TiempoSolo;
 
