import React from 'react';
import './css/tricolor.css';

export default class Tricolor extends React.Component{
    
    constructor(props){
        super(props);
        this.state = {
            estado : 3
        }
        this.pulsar = this.pulsar.bind(this);
    }
    
    pulsar(){
        let color = 3;
        color = (this.state.estado===color)? 0 : this.state.estado + 1;
        this.setState({
            estado: color
        })
    }

    render(){
        let variable = "";
        switch(this.state.estado){
            case 0:
            variable= 'tricolor gris';
            break;
            case 1:
            variable= 'tricolor rojo';
            break;
            case 2:
            variable='tricolor verde';
            break;
            case 3:
            variable='tricolor amarillo';
            break;
            default:
            console.log("error");
        }
        return (
            <div onClick={this.pulsar} className= {variable} >
            </div>
        );
    }

}